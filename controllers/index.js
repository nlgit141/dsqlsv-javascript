//* Tạo mảng bên ngoài hàm để lưu trữ thông tin sinh vien
var mangSinhVien = [];

document.getElementById("btnXacNhan").onclick = function () {
    var sinhVien = new SinhVien();
    sinhVien.maSinhVien = document.getElementById("maSinhVien").value;
    sinhVien.tenSinhVien = document.getElementById("tenSinhVien").value;
    sinhVien.loaiSinhVien = document.getElementById("loaiSinhVien").value;
    sinhVien.diemRenLuyen = document.getElementById("diemRenLuyen").value;
    sinhVien.email = document.getElementById("email").value;
    sinhVien.soDienThoai = document.getElementById("soDienThoai").value;
    sinhVien.diemToan = document.getElementById("diemToan").value;
    sinhVien.diemLy = document.getElementById("diemLy").value;
    sinhVien.diemHoa = document.getElementById("diemHoa").value;
    console.log("sinhVien", sinhVien);

    //kiểm tra dữ liệu trước khi nhập vào mảng
    var valid = true; // đặt cờ hiệu
    valid &= kiemTraRong(sinhVien.maSinhVien, "#error_required_maSinhVien", "Mã sinh viên") & kiemTraRong(sinhVien.tenSinhVien, "#error_required_tenSinhVien", "Tên sinh viên ") & kiemTraRong(sinhVien.email, "#error_email", "email ") & kiemTraRong(sinhVien.soDienThoai, "#error_all_number_soDienThoai", "Số điện thoại ") & kiemTraRong(sinhVien.diemToan, "#error_all_number_diemToan", "Điểm toán ") & kiemTraRong(sinhVien.diemLy, "#error_all_number_diemLy", "Điểm lý ") & kiemTraRong(sinhVien.diemHoa, "#error_all_number_diemHoa", "Điểm hóa ");

    // kiểm tra ký tự
    valid &= kiemTraTatCaKyTu(sinhVien.tenSinhVien, "#error_all_letter_tenSinhVien", "Tên sinh viên ");
    // kiểm tra email
    valid &= kiemTraEmail(sinhVien.Email, "#error_email", "Email ");



    if (!valid) {
        return;
    }

    //Mỗi lần nhập liệu và bấm nút xác nhận thì thêm sinh viên vào mảng
    mangSinhVien.push(sinhVien);

    //lưu vào localStorage
    luuStore(mangSinhVien, "mangSinhVien");

    //Gọi hàm tạo table sinhVien
    var html = renderSinhVien(mangSinhVien);
    document.querySelector("tbody").innerHTML = html;
};
/**
 * Hàm này sẽ nhận vào 1 array (sinhVien) và trả ra output là string <tr>...</tr>
 * @param {*} arrSinhVien  arrSinhVien là mảng các object sinhVien[sinhVien1,sinhVien2,...]
 * @returns trả ra 1 giá trị là 1 htmlString "<tr>...</tr> <tr>...</tr>"
 */


function renderSinhVien(arrSinhVien) { //param: input:arrSinhVien
    var html = "";
    for (var i = 0; i < arrSinhVien.length; i++) {
        var sv = arrSinhVien[i]; // mỗi lần duyệt lấy ra 1 object sinhVien và lưu vào 
        html += `
        <tr>
            <td>${sv.maSinhVien}</td>
            <td>${sv.tenSinhVien}</td>
            <td>${sv.email}</td>
            <td>${sv.soDienThoai}</td>
            <td>${sv.loaiSinhVien}</td>
            <td>
            <button class="btn btn-danger"> Xóa</button>
            </td>
        </tr>`;
    }
    return html;
}

/**
 * LƯU VÀO localStorage
 * hàm nhận vào 1 giá trị object hoặc array lưu vào localStorage của trình duyệt
 * @param {*} conten là object hoặc array muốn lưu
 * @param {*} storeName là tên của storage
 */
function luuStore(content, storeName) {
    var sContent = JSON.stringify(content);//Biến đổi object hoặc array thành chuỗi
    localStorage.setItem(storeName, sContent);// đem chuỗi đó lưu vào localStorage & chỉ lưu được chuỗi
}

/**
 *! hàm lấy dữ liệu từ  localStorage dựa vào storeName
 * @param {*} storeName  tên store cần lấy dữ liệu
 * @returns  trả về object hoặc array
 */

function getStorage(storeName) {
    var output;
    if (localStorage.getItem(storeName)) {
        output = JSON.parse(localStorage.getItem(storeName));
    }
    return output; //underfined
}

//! hàm sẽ chạy khi giao diện vừa load lên
window.onload = function () {
    // lấy dữ liệu từ storage
    var content = getStorage("mangSinhVien");
    console.log(content);
    // nếu có dữ liệu trong storage thì xử lý in ra ngoài giao diện html cho table
    if (content) {
        mangSinhVien = content;
        var html = renderSinhVien(content);
        document.querySelector("tbody").innerHTML = html;
    }
};
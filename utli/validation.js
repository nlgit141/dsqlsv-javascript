/**
 * Hàm kiểm tra rỗng  dựa vào giá trị người dùng nhập
 * @param {*} value là giá trị người dùng nhập
 * @param {*} selectorError heeinr thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi cho giá trị đó
 * @returns trả về giá trị hợp lệ (true) hoặc không hợp lệ (false)
 */


function kiemTraRong(value, selectorError, name) {
    if (value === "") {
        document.querySelector(selectorError).innerHTML = name + "không được bỏ trống!";
        return false;
    }
    document.querySelector(selectorError).innerHTML = "";
    return true;
};

function kiemTraTatCaKyTu(value, selectorError, name) {
    var regexLetter = /^[A-Z a-z]+$/; // nhập các ký tự a->z A->Z hoặc khoảng trống không bao gồm unicode
    if (regexLetter.test(value)) { //test nếu ok false
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + "phải là chữ cái !";
    return false;
};
function kiemTraEmail(value, selectorError, name) {
    var regexEmail =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (regexEmail.test(value)) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + "không đúng định dạng !";
    return false;
}

function kiemTraSo(value, selectorError, name) {
    var regexNumber = /^[0-9]+$/;
    if (regexNumber.test(value)) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + "Tất cả là số !";
    return false;
};